import org.junit.*;
import org.junit.Assert.*;

import static org.junit.Assert.assertEquals;

public class AppMainTest {
    @Test
    public void testMainOK(){
        int num = 3;
        assertEquals(num,AppMain.returnItself(num));
    }
}
